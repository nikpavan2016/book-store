import React from 'react'
import {FlatList,Text,StyleSheet} from 'react-native'
import { NavigationFunctionComponent } from 'react-native-navigation'
import {FilterHeader} from '../components/Headers/index'
import { Container, Content, View } from 'native-base'
import Color from '../styles/colors'
import {DirectoryList } from '../components/Directory/index'
import {popEvent} from '../NavigationEvent'
import {useSelector, useDispatch} from 'react-redux';
import {removeFromCart} from '../store/cart/action';

interface Props { }

const Cart: NavigationFunctionComponent<Props> = ({
    componentId,
}): JSX.Element => {
  const cartProduct = useSelector((state: any) => state.cartReducer.products);
  const cartCount = useSelector((state: any) => state.cartReducer.count);
  const dispatch = useDispatch();


  const renderItem = ({ item }:any) => (
    <DirectoryList
      data={item}
      componentId={componentId}   
      isCart={true}   
      onPress={()=>{dispatch(removeFromCart(item))}}
    />
  );

  return (
    <Container>
      <View style={{paddingHorizontal:10,backgroundColor:Color.grayColor1}}>
        <FilterHeader title='Cart' onPress={()=>{popEvent(componentId)}} />
      </View>        
      <Content style={{paddingHorizontal:10}}>
        <View style={{flexDirection:'row',alignItems:'center',marginVertical:10}}>
          <View style={styles.countWrapper}>
            <Text style={{color:Color.white}}>{cartCount}</Text>
          </View>
          <Text style={{fontWeight:'bold',fontSize:16,marginLeft:10}}>Added</Text>
        </View>  
        <FlatList
          keyExtractor={(item:any,index) => item.id.toString()}
          data={cartProduct}
          renderItem={renderItem}
          style={{paddingTop:10}}
        />
      </Content>  
    </Container>  
  )
}

const styles = StyleSheet.create({
  countWrapper:{
    height:30,
    width:30,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:Color.orange,
    borderRadius:5
  }
})

export default Cart
