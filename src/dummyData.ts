export const books = [{
  id: 1,
  name: 'The Book Thief',
  author: 'Markus Zusak',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1522157426l/19063._SY475_.jpg' 
},
{
  id: 2,
  name: 'Sapiens',
  author: 'Yuval Noah Harari',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1420585954l/23692271.jpg'
},
{
  id: 3,
  name: 'Crime and Punishment',
  author: 'Fyodor Dostoyevsky',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1382846449l/7144.jpg'
},
{
  id: 4,
  name: 'No Longer Human',
  author: 'Osamu Dazai',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1422638843l/194746.jpg'
},
{
  id: 5,
  name: 'Atomic Habits',
  author: 'James Clear',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1535115320l/40121378._SY475_.jpg'
},
{
  id: 7,
  name: 'Dune',
  author: 'Frank Herbert',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1434908555l/234225._SY475_.jpg'
},
{
  id: 8,
  name: 'Atlas Shrugged',
  author: 'Ayn Rand',
  quantity:1,
  imgUrl: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1405868167l/662.jpg'
}
]