import {
  StyleSheet,
} from 'react-native'

import baseStyles from '../../styles/base'

const homeComponentStyles = StyleSheet.create({
  image:{
    width:72,
    height:72,
  },
})

export default {
  ...homeComponentStyles,
  ...baseStyles,
}