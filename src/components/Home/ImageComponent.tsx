import React, { useCallback } from 'react';
import FastImage from 'react-native-fast-image'
import styles from './styles'


type ImageType = {
  imageUri: any,
  style?: any
}

const ImageView: React.FC<ImageType> = ({
  imageUri, style
}): JSX.Element => {


  return (
    <FastImage
      style={style ? style :styles.image}
      source={{
        uri: imageUri,
        priority: FastImage.priority.high,
        cache: FastImage.cacheControl.immutable
      }}
      resizeMode={FastImage.resizeMode.cover}
    />
  );
};




export default ImageView;
