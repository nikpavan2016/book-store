import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from './styles'
import Color from '../../styles/colors'
import {useSelector, useDispatch} from 'react-redux';


type props = {
  componentId?:any
  title?:string,
  style?:any,
  onPress?:any,
}

const Header: React.FC<props> = ({
  componentId,
  title,
  style,
  onPress,
}): JSX.Element => {
  const cartCount = useSelector((state: any) => state.cartReducer.count);


  return (
    <View style={[{flexDirection:'row',marginVertical:20,position:'relative'},style]}>
      <View style={{flex:1,}}>
        <Text style={styles.headerStyle}>{title}</Text>
      </View>
      <View style={styles.countWrapper}>
        <Text style={{color:Color.white,fontSize:12}}>{cartCount}</Text>
      </View>
      <TouchableOpacity onPress={onPress} style={{flexDirection:'row',}}>
        <Icon
          name='cart'
          type='Ionicons'
          style={{color:Color.primaryColor}}
        />
      </TouchableOpacity>
    </View>
  )
};

export default Header
