import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from './styles'
import Color from '../../styles/colors'
import {useSelector, useDispatch} from 'react-redux';


type props = {
  componentId?:any
  title?:string,
  style?:any,
  onPress?:any,
  isCloseButton?:boolean,
  onPressFilter?:any,
  onPressSearch?:any,
  isFilter?:boolean
}

const FilterHeader: React.FC<props> = ({
  title,
  style,
  onPress,
  isCloseButton,
}): JSX.Element => {

  return (
    <View style={[{flexDirection:'row',marginVertical:20},style]}>
      <TouchableOpacity onPress={onPress} >
        <Icon
          name='arrow-back-outline'
          type='Ionicons'
          style={{color:Color.primaryColor}}
        />
      </TouchableOpacity>
      <View style={{alignItems:'center',flex:1,justifyContent:'center'}}>
        <Text style={styles.headerStyle}>{title}</Text>
      </View>
      <View style={{alignItems:'center',flexDirection:'row'}}>
       </View>
    </View>
  )
};

export default FilterHeader
