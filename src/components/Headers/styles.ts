import {StyleSheet} from 'react-native'
import baseStyles from '../../styles/base'
// import * as fontConfig from '../../styles/variables'
import fontConfig from '../../styles/variables'
import Color from '../../styles/colors'

const headerStyle = StyleSheet.create({
  headerStyle:{
    fontFamily: fontConfig.fontFamilyBold,
    fontSize: fontConfig.largeFont1,
  },
  headerWrapper:{
    marginVertical:20,
    justifyContent:'center',
    alignItems:'center'
  },
  countWrapper:{
    backgroundColor:Color.orange,
    minHeight:22,
    minWidth:22,
    borderRadius:22/2,
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    right:26,
    bottom:15,      
  }
})


export default {
  ...headerStyle,
  ...baseStyles,
}