import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import Color from '../../styles/colors'
import {Icon} from 'native-base'
import styles from './styles'
import ImageView from '../Home/ImageComponent'

type props = {
  onPress?:any,
  style?:any,
  disabled?:any,
  data:any,
  componentId?:any,
  isCart?:any
}

const DirectoryList: React.FC<props> = ({
  onPress,
  data,
  style,
  disabled,
  componentId,
  isCart
}): JSX.Element => {
  return (
    <View style={[styles.directoryListWrapper,{marginBottom:16}]}>
      <View style={{flexDirection:'row'}}>
        <ImageView style={styles.userImage} imageUri={data.imgUrl}/>
      <View style={{marginLeft:16}}>
        <Text style={[styles.header2,styles.eventTitle]}>{data.name}</Text>
        <Text style={[styles.body,{color:Color.black}]}>{data.author}</Text>
        {isCart?<Text style={[styles.body,{color:Color.black,marginTop:5}]}>Quantity: {data.quantity}</Text>:null}        
      </View>
      </View>   
      <TouchableOpacity style={styles.callIconWrapper} onPress={onPress}>
        <Icon type='Ionicons' name={isCart?'remove-outline':'add'} style={styles.callIcon}/>
      </TouchableOpacity> 
    </View>
  )
};


export default DirectoryList
