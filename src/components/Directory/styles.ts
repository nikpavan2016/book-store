import {
  StyleSheet
} from 'react-native'

import baseStyles from '../../styles/base'
// import * as fontConfig from '../../styles/variables'
import fontConfig from '../../styles/variables'
import Color from '../../styles/colors'

const directoryList = StyleSheet.create({
  directoryListWrapper:{
    padding:16,
    borderColor:Color.borderColor,
    borderWidth:0.6,
    backgroundColor:Color.white,
    flexDirection:'row',
    flex:1,
    justifyContent:'space-between'
  },

  eventTitle:{
    marginBottom:5,
    color:Color.black
  },

  eventCont:{
    color:Color.primaryColor,
    marginTop:8,
  },

  currentEventIndicator:{
    width:40,
    height:3,
    backgroundColor:Color.primaryColor,
    marginRight:6
  },
  nextEventIndicator:{
    width:40,
    height:3,
    backgroundColor:Color.grayColor1,
    marginRight:6
  },
  userImage:{
    width:50,
    height:50,
    
  },
  callIconWrapper:{
    width:28,
    height:28,
    backgroundColor:Color.primaryColor,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:4,
    alignSelf:'flex-end'
  },
  callIcon:{
    color:Color.white,
    fontSize:15,
  }

})

export default {
  ...directoryList,
  ...baseStyles,
}