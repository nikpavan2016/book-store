import { Navigation } from 'react-native-navigation'

export const popEvent = (componentId?: any) => {
  Navigation.pop(componentId)
}

export const pushEvent = (componentId?: any, route?: any, props?: any) =>{
  Navigation.push(componentId, {
    component: {
      name: route,
      passProps: props,
      options: {
        bottomTabs: {
          visible: false,
          drawBehind: true,
          animate: true
        }
      }
    },
  })
}

export const showModal = (componentId?: any, route?: any, props?: any) => {
  Navigation.showModal({
    stack: {
      children: [
        {
          component: {
            name: route,
          },
        },
      ],
    },
  })
}

export const dismissModal = (componentId?: any) => {
  Navigation.dismissModal(componentId)
}

