import { Navigation } from 'react-native-navigation';
import { BOOKLIST } from './screens';
import {registerScreens} from './routes';

registerScreens()

export const startApp = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [{component: {name: BOOKLIST}}]}
      }
  });
};

