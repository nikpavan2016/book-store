const colors:any = {
  primaryColor: '#18248A',
  lightPrimaryColor:'rgba(24,36,138,0.4)',
  white: '#FFFFFF',
  black:'#333333',
  yellow: '#db9c14',
  orange: '#ffa500',
  red:'#FF0000',
  grayColor1: '#F4F5F5',
  lightBlack:'#000000',
  borderColor:'#E5E5E5',
  lightBorderColor:'rgba(51,51,51,0.6)',
  lightBlackColor:'rgba(51,51,51,0.2)',
  green:'rgba(78,169,1,0.2)',
  lightRed:'rgba(255,0,0,0.2)',
  lightOrange:'rgba(223,167,115,0.2)'
}

export default colors;
