import {
  StyleSheet
} from 'react-native'

// import * as vars from './variables'
import fontConfig from './variables'
import Color from '../styles/colors'


export const fontStyles = StyleSheet.create({
  //bold:600, font:12
  tinyFonts:{
    fontFamily: fontConfig.fontFamilySemiBold,
    fontSize: fontConfig.tinyFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:600, font:14
  subHeader: {
    fontFamily: fontConfig.fontFamilySemiBold,
    fontSize: fontConfig.smallFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  // 600,font:16
  subHeader1: {
    fontFamily: fontConfig.fontFamilySemiBold,
    fontSize: fontConfig.normalFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  // 600, font:18
  subHeader2: {
    fontFamily: fontConfig.fontFamilySemiBold,
    fontSize: fontConfig.mediumFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  // semi-bold:600, font:24
  header1: {
    fontFamily: fontConfig.fontFamilySemiBold,
    fontSize: fontConfig.largeFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:300, font:16
  normalFont:{
    fontFamily: fontConfig.fontFamilyNormal,
    fontSize: fontConfig.normalFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:300, font:12
  smallFont:{
    fontFamily: fontConfig.fontFamilyNormal,
    fontSize: fontConfig.tinyFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:300, font:14
  body:{
    fontFamily: fontConfig.fontFamilyNormal,
    fontSize: fontConfig.smallFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:700, font:14
  subTitle: {
    fontFamily: fontConfig.fontFamilyBold,
    fontSize: fontConfig.smallFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  //bold:700, font:18
  header2: {
    fontFamily: fontConfig.fontFamilyBold,
    fontSize: fontConfig.mediumFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },

  // bold:700, font:28
  bigFont:{
    fontFamily: fontConfig.fontFamilyBold,
    fontSize: fontConfig.bigFont,
    letterSpacing:0.3,
    color:Color.lightBlack
  },







})