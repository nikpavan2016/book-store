
const fontConfig:any = {
  tinyFont : 12,
  smallFont : 14,
  normalFont : 16,
  mediumFont : 18,
  largeFont1 : 22,
  largeFont : 24,
  bigFont : 28,
  tinyWidth : 0.6,
  fontFamilyBold: 'OpenSans-Bold', // '700'
  fontFamilySemiBold: 'OpenSans-SemiBold', // '600'
  fontFamilyNormal:'OpenSans-Regular' // '300'
}

export default fontConfig;
