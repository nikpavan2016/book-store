import {
  StyleSheet
} from 'react-native'

import { fontStyles } from './fonts'

const baseStyles = StyleSheet.create({

})

export default {
  ...fontStyles,
  ...baseStyles,
}