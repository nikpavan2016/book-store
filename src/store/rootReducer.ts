import { combineReducers } from 'redux';

import cartReducer from './cart/reducers'
const rootReducer = combineReducers({
    cartReducer
});

export default rootReducer;
