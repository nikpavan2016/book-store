import {ADD_TO_CART,REMOVE_FROM_CART} from './action'

const initialState: any = {
  products: [],
  count: 0,
};
  
async function updateExistingProduct(
  action: any,
  products: any[],
  existingIndex: number,
  ) {  
  if (action.bookData) {
    products[existingIndex].quantity = products[existingIndex].quantity + 1;
  } else {
    products[existingIndex].quantity = action.bookData.quantity;
  }
}

async function removeExistingProduct(
  action: any,
  products: any[],
  existingIndex: number,
  ) {  
  if (action.bookData) {
    if(products[existingIndex].quantity > 1) {
      products[existingIndex].quantity = products[existingIndex].quantity - 1;
    }else {
      products.splice(existingIndex, 1);
    }    
  } 
}
    
function getProductCount(products: any[]) {
  let count = 0;
  products.forEach(product => {
    count += product.quantity;
  });
  return count;
}
  
function getUpdatedData(
  action: any, 
  state: any, 
  existingIndex: any,
  isRemoveAction?:boolean
) {
  if(isRemoveAction){
    removeExistingProduct(action, state.products, existingIndex);  
  }else {
    updateExistingProduct(action, state.products, existingIndex);
  }
  return {
    ...state,     
    count: getProductCount([...state.products]),
  };
}
  
function getAddedData(state: any, action: any) {
  let returnRes = {
    ...state,
    products: [...state.products, action.bookData],
    count: getProductCount([...state.products, action.bookData]),
  }
  return returnRes;  
}
  
  const cartReducer = (state = initialState, action: any) => {
    switch (action.type) {    
      case ADD_TO_CART:
        if (action.bookData) {
          const existingIndex = state.products.findIndex(
            (e: {id: number}) => e.id === action.bookData.id,     
          );
          if (existingIndex >= 0) {
            let updateData = getUpdatedData(action, state, existingIndex);
            return updateData;
          } else {
            let addedData = getAddedData(state, action);
            return addedData
          }
        } 
  
      case REMOVE_FROM_CART:
        const elementIndex = state.products.findIndex(
          (e: {id: string}) => e.id === action.bookData.id,
        );
        
        if (elementIndex >= 0) {
          let updateData = getUpdatedData(action, state, elementIndex,true);
          return updateData;
        }
        return {
          ...state,          
          count: getProductCount([...state.products]),
        };
  
      default:return state;
    }
  };
  
  export default cartReducer;
  