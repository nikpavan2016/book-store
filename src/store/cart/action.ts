export const ADD_TO_CART = 'ADD_TO_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'

export const addToCart=(bookData:any):AddToCartAction => ({    
    type:ADD_TO_CART,
    bookData
})

export const removeFromCart=(bookData:any):AddToCartAction => ({
    type:REMOVE_FROM_CART,
    bookData
})

