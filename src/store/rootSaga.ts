import { all } from 'redux-saga/effects';

// Define saga functions
export default function* rootSaga() {
    yield all([]);
}
