import { Navigation } from 'react-native-navigation';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import { withReduxProvider } from './store';

import {BOOKLIST,CART} from './screens';

import Cart from './screens/Cart'
import BookList from './screens/BookList'

export const registerScreens = (): void => {
  const Screens = new Map<string, React.FC<any>>();

  Screens.set(BOOKLIST, BookList);
  Screens.set(CART,Cart)

  // Register screens
  Screens.forEach((C, key) => {
    Navigation.registerComponent(key,
      () => gestureHandlerRootHOC(withReduxProvider(C)),
      () => C,
    );
  });

}