import 'expo-asset';
import { Navigation, } from 'react-native-navigation';
import { startApp } from './src/App';

Navigation.events().registerAppLaunchedListener(() => {
  console.disableYellowBox = true;
    Navigation.setDefaultOptions({
      topBar: {
        visible: false,
        drawBehind: false,
      },
    });
  startApp();
});